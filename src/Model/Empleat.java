package Model;

public class Empleat {
    //EMP_NO, COGNOM, OFICI, CAP, DATA_ALTA, SALARI, COMISSIO, DEPT_NO
    private int empleatId;
    private String cognom;
    private String ofici;
    private int cap;
    private String dataAlta;
    private int salari;
    private int comissio;
    private int depId;

    public Empleat(int empleatId, String cognom, String ofici, int cap, String dataAlta, int salari, int comissio, int depId) {
        this.empleatId = empleatId;
        this.cognom = cognom;
        this.ofici = ofici;
        this.cap = cap;
        this.dataAlta = dataAlta;
        this.salari = salari;
        this.comissio = comissio;
        this.depId = depId;
    }

    public int getEmpleatId() {
        return empleatId;
    }

    public void setEmpleatId(int empleatId) {
        this.empleatId = empleatId;
    }

    public String getCognom() {
        return cognom;
    }

    public void setCognom(String cognom) {
        this.cognom = cognom;
    }

    public String getOfici() {
        return ofici;
    }

    public void setOfici(String ofici) {
        this.ofici = ofici;
    }

    public int getCap() {
        return cap;
    }

    public void setCap(int cap) {
        this.cap = cap;
    }

    public String getDataAlta() {
        return dataAlta;
    }

    public void setDataAlta(String dataAlta) {
        this.dataAlta = dataAlta;
    }

    public int getSalari() {
        return salari;
    }

    public void setSalari(int salari) {
        this.salari = salari;
    }

    public int getComissio() {
        return comissio;
    }

    public void setComissio(int comissio) {
        this.comissio = comissio;
    }

    public int getDepId() {
        return depId;
    }

    public void setDepId(int depId) {
        this.depId = depId;
    }

    @Override
    public String toString() {
        return "Empleat{" +
                "empleatId=" + empleatId +
                ", cognom='" + cognom + '\'' +
                ", ofici='" + ofici + '\'' +
                ", cap=" + cap +
                ", dataAlta='" + dataAlta + '\'' +
                ", salari=" + salari +
                ", comissio=" + comissio +
                ", depId=" + depId +
                '}';
    }
}
