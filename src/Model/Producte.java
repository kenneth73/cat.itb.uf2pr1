package Model;

public class Producte {
    private int prodId;
    private String descripcio;

    public Producte(int prodId, String descripcio) {
        this.prodId = prodId;
        this.descripcio = descripcio;
    }

    public int getProdId() {
        return prodId;
    }

    public void setProdId(int prodId) {
        this.prodId = prodId;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    @Override
    public String toString() {
        return "Producte{" +
                "prodId=" + prodId +
                ", descripcio='" + descripcio + '\'' +
                '}';
    }
}
