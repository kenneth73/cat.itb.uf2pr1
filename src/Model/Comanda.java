package Model;

public class Comanda {

    private int comNum;
    private String comData;
    private char comTipus;
    private int codClient;
    private String dataTramesa;
    private Double total;

    public Comanda(int comNum, String comData, char comTipus, int codClient, String dataTramesa, Double total) {
        this.comNum = comNum;
        this.comData = comData;
        this.comTipus = comTipus;
        this.codClient = codClient;
        this.dataTramesa = dataTramesa;
        this.total = total;
    }

    public int getComNum() {
        return comNum;
    }

    public void setComNum(int comNum) {
        this.comNum = comNum;
    }

    public String getComData() {
        return comData;
    }

    public void setComData(String comData) {
        this.comData = comData;
    }

    public char getComTipus() {
        return comTipus;
    }

    public void setComTipus(char comTipus) {
        this.comTipus = comTipus;
    }

    public int getCodClient() {
        return codClient;
    }

    public void setCodClient(int codClient) {
        this.codClient = codClient;
    }

    public String getDataTramesa() {
        return dataTramesa;
    }

    public void setDataTramesa(String dataTramesa) {
        this.dataTramesa = dataTramesa;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Comanda{" +
                "comNum=" + comNum +
                ", comData='" + comData + '\'' +
                ", comTipus=" + comTipus +
                ", codClient=" + codClient +
                ", dataTramesa='" + dataTramesa + '\'' +
                ", total=" + total +
                '}';
    }
}
