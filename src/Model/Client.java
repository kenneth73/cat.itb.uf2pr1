package Model;

public class Client {
    private int clientId;
    private String nom;
    private String adreca;
    private String ciutat;
    private String  estat;
    private int codiPostal;
    private int area;
    private String telefon;
    private int  reprCod;
    private float limitCredit;
    private String observacions;

    public Client(int clientId, String nom, String adreca, String ciutat, String estat, int codiPostal, int area, String telefon, int reprCod, float limitCredit, String observacions) {
        this.clientId = clientId;
        this.nom = nom;
        this.adreca = adreca;
        this.ciutat = ciutat;
        this.estat = estat;
        this.codiPostal = codiPostal;
        this.area = area;
        this.telefon = telefon;
        this.reprCod = reprCod;
        this.limitCredit = limitCredit;
        this.observacions = observacions;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdreca() {
        return adreca;
    }

    public void setAdreca(String adreca) {
        this.adreca = adreca;
    }

    public String getCiutat() {
        return ciutat;
    }

    public void setCiutat(String ciutat) {
        this.ciutat = ciutat;
    }

    public String getEstat() {
        return estat;
    }

    public void setEstat(String estat) {
        this.estat = estat;
    }

    public int getCodiPostal() {
        return codiPostal;
    }

    public void setCodiPostal(int codiPostal) {
        this.codiPostal = codiPostal;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public int getReprCod() {
        return reprCod;
    }

    public void setReprCod(int reprCod) {
        this.reprCod = reprCod;
    }

    public float getLimitCredit() {
        return limitCredit;
    }

    public void setLimitCredit(float limitCredit) {
        this.limitCredit = limitCredit;
    }

    public String getObservacions() {
        return observacions;
    }

    public void setObservacions(String observacions) {
        this.observacions = observacions;
    }

    @Override
    public String toString() {
        return "Client{" +
                "clientId=" + clientId +
                ", nom='" + nom + '\'' +
                ", adreca='" + adreca + '\'' +
                ", ciutat='" + ciutat + '\'' +
                ", estat='" + estat + '\'' +
                ", codiPostal=" + codiPostal +
                ", area=" + area +
                ", telefon='" + telefon + '\'' +
                ", reprCod=" + reprCod +
                ", limitCredit=" + limitCredit +
                ", observacions='" + observacions + '\'' +
                '}';
    }
}
