import ConnexionsBD.ConnexioMySQL;
import MetodesCRUD.Client_CRUD;
import MetodesCRUD.Comanda_CRUD;
import MetodesCRUD.Empleat_CRUD;
import MetodesCRUD.Producte_CRUD;
import Model.Client;
import Model.Empleat;
import Model.Producte;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static ConnexioMySQL connexioMySQL = new ConnexioMySQL();
    public static Connection connection = null;
    public static Client_CRUD clientCrud;
    public static Empleat_CRUD empleatCrud;
    public static Producte_CRUD producteCrud;
    public static Comanda_CRUD comanda_crud;
    public static void main(String[] args) {
        try {
            connection = connexioMySQL.conectar();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        clientCrud = new Client_CRUD(connection);
        empleatCrud = new Empleat_CRUD(connection);
        producteCrud = new Producte_CRUD(connection);
        comanda_crud = new Comanda_CRUD(connection);
        //executaMenu();

        /***** EXAMEN 01/02/2021 *****/
        comanda_crud.selectComandaById(618);
        comanda_crud.updateComandaTipus(611, 'A');
        comanda_crud.selectComandaById(611);

    }

    public static void exercici1EjecutarScriptMySQL(String pathScript) {
        File scriptFile = new File(pathScript);
        System.out.println("Fichero a ejecutar : " + scriptFile.getName());
        System.out.println("Convirtiendo el fichero a cadena...");
        BufferedReader entrada = null;
        try {
            entrada = new BufferedReader(new FileReader(scriptFile));
        } catch (FileNotFoundException e) {
            System.out.println("ERROR NO HAY FILE: " + e.getMessage());
        }
        String linea = null;
        StringBuilder stringBuilder = new StringBuilder();
        String salto = System.getProperty("line.separator");
        try {
            while ((linea = entrada.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(salto);
            }
        } catch (IOException e) {
            System.out.println("ERROR de E/S, al operar " + e.getMessage());
        }
        String consulta = stringBuilder.toString();
        //System.out.println(consulta);

        try {
            Connection connmysql = DriverManager.getConnection("jdbc:mysql://192.168.1.138:3306/empresa?allowMultiQueries=true","user1", "password1");
            Statement sents = connmysql.createStatement();
            int res = sents.executeUpdate(consulta);
            System.out.println("Script ejecutado con éxito");
            connmysql.close();
            sents.close();
        } catch (SQLException e) {
            System.out.println("ERROR AL EJECUTAR EL SCRIPT: " + e.getMessage());
        }
    }

    public static void exercici2InsertaProductes() {
        int[] prodNums = {300388, 400552, 400333};
        String[] descripcions = {"RH GUIDE TO PADDLEx", "RH GUIDE TO BOxX", "ACE TENNIS BALLS-10 PACxK"};
        int filasAfectadas= 0;
        for (int i = 0; i <prodNums.length ; i++) {
            Producte p = new Producte(prodNums[i], descripcions[i]);
            filasAfectadas += producteCrud.insertProducte(p);
        }
        System.out.println("Filas insertades: " + filasAfectadas);
    }

    public static void exercici3InsertarEmpleats() {
        int[] empNos = {4885 , 8772, 9945};
        String[] cognoms = {"BORREL", "PUIG", "FERRER"};
        String[] oficis = {"EMPLEAT", "VENEDOR", "ANALISTA"};
        int[] caps = {7902 , 7698, 7698};
        String[] dataAltas = {"1981-12-25", "1990-01-23 ", "1988-05-17"};
        int[] salaris = {104000 , 108000, 169000};
        int[] comissions = {0, 0, 39000};
        int[] depsNos = {30, 30, 20};

        int filas= 0;
        for (int i = 0; i <empNos.length ; i++) {
            Empleat e = new Empleat(empNos[i], cognoms[i], oficis[i], caps[i], dataAltas[i], salaris[i], comissions[i], depsNos[i]);
            filas+=empleatCrud.insertEmp(e);
        }
        System.out.println("Filas insertades: " + filas);
    }

    public static void exercici4ActualitzaCredit() {
        //dades a actualitzar
        int[] clientCod = {104 , 106, 107};
        float[] limitCredit = {20000 , 12000, 20000};
        int filasAfectades= 0;
        for (int i = 0; i <clientCod.length ; i++) {
            filasAfectades += clientCrud.updateCredit(clientCod[i], limitCredit[i]);
        }
        System.out.println("Filas modificades: " + filasAfectades);
    }

    public static void exercici5ConsultaClient(int id) {
        Client c = clientCrud.selectClient(id);
        System.out.println(c.toString());
    }

    public static void exercici6ConsultaEmpleatPerId(int id) {
        Empleat e = empleatCrud.selectEmp(id);
        System.out.println(e.toString());
    }

    public static void exercici7ConsultaProductePerId(int id) {
        Producte p = producteCrud.selectProd(id);
        System.out.println(p.toString());
    }

    public static void exercici8EliminaClientPerId(int id) {
        if (clientCrud.deleteClient(id) == 1)
            System.out.println("Client amb id " + id + " eliminat correctament");
        else
            System.out.println("No s'ha pogut esborrar el client amb id " + id );

    }

    public static void exercici9EliminaEmpleatPerId(int id) {
        if (empleatCrud.deleteEmp(id) == 1)
            System.out.println("Empleat amb id " + id + " eliminat correctament");
        else System.out.println("No s'ha pogut esborrar l'empleat amb id " + id );
    }

    public static void exercici10EliminaProductePerId(int id) {
        if (producteCrud.deleteProd(id) == 1)
            System.out.println("Producte amb id " + id + " eliminat correctament");
        else System.out.println("No s'ha pogut esborrar el producte amb id " + id );
    }

    //FUNCIONALITAT MENU
    public static void printMenu() {
        System.out.println("******************");
        System.out.println("[1] = Executa l’script 'empresa.sql'");
        System.out.println("[2] = Inserta nous productes utilitzant el PreparedStatement");
        System.out.println("[3] = Inserta nous empleats");
        System.out.println("[4] = Actualitza el límit de crèdit dels següents clients utilitzant el PreparedStatement");
        System.out.println("[5] = Consulta les dades del client 106");
        System.out.println("[6] = Consulta les dades de l’empleat 7788");
        System.out.println("[7] = Consulta les dades del producte 101860 utilitzant el PreparedStatement");
        System.out.println("[8] = Eliminar el client amb codi 109 utilitzant el PreparedStatement");
        System.out.println("[9] = Actualitza el límit de crèdit dels següents clients");
        System.out.println("[10] = Eliminar el producte 400552 utilitzant el PreparedStatement");
        System.out.println("[0] = Sortir");
        System.out.println("******************");
    }

    public static void executaMenu() {
        int opcio;
        while (true){
            printMenu();
            opcio = inputInt("Introdueix un numero: ");
            switch (opcio) {
                case 0:
                    break;
                case 1:
                    exercici1EjecutarScriptMySQL("empresa.sql");
                    break;
                case 2:
                    exercici2InsertaProductes();
                    break;
                case 3:
                    exercici3InsertarEmpleats();
                    break;
                case 4:
                    exercici4ActualitzaCredit();
                    break;
                case 5:
                    exercici5ConsultaClient(106);
                    break;
                case 6:
                    exercici6ConsultaEmpleatPerId(7788);
                    break;
                case 7:
                    exercici7ConsultaProductePerId(101860);
                    break;
                case 8:
                    exercici8EliminaClientPerId(109);
                    break;
                case 9:
                    exercici9EliminaEmpleatPerId(4885);
                    break;
                case 10:
                    exercici10EliminaProductePerId(400552);
                    break;
                default:
                    System.out.println("Opció no vàlida");
                    break;
            }
        }
    }

    public static int inputInt(String m){
        Scanner scan = new Scanner(System.in);
        boolean correct;
        int num = 0;
        do{
            System.out.println(m);
            if(correct = scan.hasNextInt()){
                num = scan.nextInt();
            }else{
                scan.nextLine();
            }
        }while(!correct || num <=0);
        return num;
    }
}
