package MetodesCRUD;


import Model.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Client_CRUD {
    public Connection connection = null;

    public Client_CRUD(Connection connection) {
        this.connection  = connection;
    }

    public int insertClient(Client c){
        int fila= 0;
        try {
            String sql = "INSERT INTO CLIENT VALUES " + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement sentenciaSql = connection.prepareStatement(sql);
            sentenciaSql.setInt(1,c.getClientId());
            sentenciaSql.setString(2, c.getNom());
            sentenciaSql.setString(3, c.getAdreca());
            sentenciaSql.setString(4, c.getCiutat());
            sentenciaSql.setString(5, c.getEstat());
            sentenciaSql.setInt(6, c.getCodiPostal());
            sentenciaSql.setInt(7, c.getArea());
            sentenciaSql.setString(8, c.getTelefon());
            sentenciaSql.setInt(7, c.getReprCod());
            sentenciaSql.setFloat(7, c.getLimitCredit());
            sentenciaSql.setString(7, c.getObservacions());
            fila = sentenciaSql.executeUpdate();
            sentenciaSql.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return fila;
    }

    public int updateClient(Client c){ return 1; }

    public Client selectClient(int id){
        Client c = null;
        try {
            // construir orden SELECT
            String sql= "SELECT * FROM CLIENT WHERE CLIENT_COD=? ORDER BY 1";
            PreparedStatement sentenciaSql = connection.prepareStatement(sql);
            sentenciaSql.setInt(1, id);
            ResultSet rs = sentenciaSql.executeQuery();
            while (rs.next())
                c = new Client(rs.getInt("CLIENT_COD"),
                        rs.getString("NOM"),
                        rs.getString("ADRECA"),
                        rs.getString("CIUTAT"),
                        rs.getString("ESTAT"),
                        rs.getInt("codi_postal"),
                        rs.getInt("area"),
                        rs.getString("TELEFON"),
                        rs.getInt("REPR_COD"),
                        rs.getInt("LIMIT_CREDIT"),
                        rs.getString("OBSERVACIONS"));
            rs.close();
            sentenciaSql.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return c;
    }

    public int deleteClient(int id){
        int res = 0;
        try {
            String sql= "DELETE FROM CLIENT WHERE CLIENT_COD=?";
            PreparedStatement sentenciaSql = connection.prepareStatement(sql);
            sentenciaSql.setInt(1, id);
            res = sentenciaSql.executeUpdate();
            sentenciaSql.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

    public int updateCredit(int id, float limit) {
        int filaAfectada= 0;
        try {
            String sql = ("UPDATE CLIENT SET LIMIT_CREDIT=?  WHERE CLIENT_COD=?");
            PreparedStatement sentenciaSql = connection.prepareStatement(sql);
            sentenciaSql.setFloat(1, limit);
            sentenciaSql.setInt(2,id);
            filaAfectada = sentenciaSql.executeUpdate();
            sentenciaSql.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return filaAfectada;
    }
}
