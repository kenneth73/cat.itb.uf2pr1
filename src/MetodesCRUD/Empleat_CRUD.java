package MetodesCRUD;

import Model.Empleat;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Empleat_CRUD {
    public Connection connection = null;

    public Empleat_CRUD(Connection connection) {
        this.connection  = connection;
    }

    public int insertEmp(Empleat e){
        int fila= 0;
        try {
            String sql = "INSERT INTO EMP VALUES " + "(?, ?, ?, ?, ?, ?, ?, ?)";
            PreparedStatement sentenciaSql = connection.prepareStatement(sql);
            sentenciaSql.setInt(1,e.getEmpleatId());
            sentenciaSql.setString(2, e.getCognom());
            sentenciaSql.setString(3, e.getOfici());
            sentenciaSql.setInt(4, e.getCap());
            sentenciaSql.setString(5, e.getDataAlta());
            sentenciaSql.setInt(6, e.getSalari());
            sentenciaSql.setInt(7, e.getComissio());
            sentenciaSql.setInt(8, e.getDepId());
            fila = sentenciaSql.executeUpdate();
            sentenciaSql.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return fila;
    }

    public int updateEmp(Empleat e){
        return 1;
    }

    public Empleat selectEmp(int id){
        Empleat e = null;
        try {
            String sql= "SELECT * FROM EMP WHERE EMP_NO=? ORDER BY 1";
            PreparedStatement sentenciaSql = connection.prepareStatement(sql);
            sentenciaSql.setInt(1, id);
            ResultSet rs = sentenciaSql.executeQuery();
            while (rs.next())
                e = new Empleat(rs.getInt("EMP_NO"),
                        rs.getString("COGNOM"),
                        rs.getString("OFICI"),
                        rs.getInt("CAP"),
                        rs.getString("DATA_ALTA"),
                        rs.getInt("SALARI"),
                        rs.getInt("COMISSIO"),
                        rs.getInt("DEPT_NO"));
            rs.close();
            sentenciaSql.close();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return e;
    }

    public int deleteEmp(int id){
        int res = 0;
        try {
            String sql= "DELETE FROM EMP WHERE EMP_NO=?";
            PreparedStatement sentenciaSql = connection.prepareStatement(sql);
            sentenciaSql.setInt(1, id);
            res = sentenciaSql.executeUpdate();
            sentenciaSql.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }
}
