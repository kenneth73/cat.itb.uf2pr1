package MetodesCRUD;

import Model.Client;
import Model.Comanda;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Comanda_CRUD {

    public Connection connection = null;

    public Comanda_CRUD(Connection connection) {
        this.connection  = connection;
    }

    public void selectComandaById(int id){
        Comanda c = null;
        try {
            String sql= "SELECT * FROM COMANDA WHERE COM_NUM=?";
            PreparedStatement sentenciaSql = connection.prepareStatement(sql);
            sentenciaSql.setInt(1, id);
            ResultSet rs = sentenciaSql.executeQuery();
            while (rs.next())
                c = new Comanda(rs.getInt("COM_NUM"),
                        rs.getString("COM_DATA"),
                        rs.getString("COM_TIPUS").charAt(0),
                        rs.getInt("CLIENT_COD"),
                        rs.getString("DATA_TRAMESA"),
                        rs.getDouble("TOTAL"));
            rs.close();
            sentenciaSql.close();
            System.out.println(c.toString());
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }



    public void updateComandaTipus(int id, char nouTipus) {
        int filaAfectada= 0;
        String tipusStr = ""+ nouTipus;
        try {
            String sql = ("UPDATE COMANDA SET COM_TIPUS=?  WHERE COM_NUM=?");
            PreparedStatement sentenciaSql = connection.prepareStatement(sql);
            sentenciaSql.setString(1, tipusStr);
            sentenciaSql.setInt(2,id);
            filaAfectada = sentenciaSql.executeUpdate();
            sentenciaSql.close();
            System.out.println("ACTUALITZACIO CORRECTA FILAS AFECTADES: "+filaAfectada);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
