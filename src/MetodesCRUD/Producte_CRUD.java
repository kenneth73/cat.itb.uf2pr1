package MetodesCRUD;

import Model.Producte;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Producte_CRUD {

    public Connection connection = null;

    public Producte_CRUD(Connection connection) {
        this.connection  = connection;
    }

    public int insertProducte(Producte p){
        int filaAfectada= 0;
        try {
            // construir orden INSERT
            String sql = "INSERT INTO PRODUCTE VALUES " + "( ?, ?)";
            PreparedStatement sentenciaSql = connection.prepareStatement(sql);
            sentenciaSql.setInt(1,p.getProdId());
            sentenciaSql.setString(2, p.getDescripcio());
            filaAfectada = sentenciaSql.executeUpdate();
            sentenciaSql.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return filaAfectada;
    }

    public int updateProd(Producte p){
        return 1;
    }

    public Producte selectProd(int id){
        Producte p = null;

        try {
            // construir orden SELECT
            String sql= "SELECT * FROM PRODUCTE WHERE PROD_NUM=? ORDER BY 1";
            PreparedStatement sentenciaSql = connection.prepareStatement(sql);
            sentenciaSql.setInt(1, id);
            ResultSet rs = sentenciaSql.executeQuery();
            while (rs.next())
                p = new Producte(rs.getInt("PROD_NUM"), rs.getString("DESCRIPCIO"));
            rs.close();
            sentenciaSql.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return p;
    }

    public int deleteProd(int id){
        int res =0;
        try {
            String sql= "DELETE FROM PRODUCTE WHERE PROD_NUM=?";
            PreparedStatement sentenciaSql = connection.prepareStatement(sql);
            sentenciaSql.setInt(1, id);
            res = sentenciaSql.executeUpdate();
            sentenciaSql.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return res;
    }

}
