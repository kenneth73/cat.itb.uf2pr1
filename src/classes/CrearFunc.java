package classes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;

public class CrearFunc {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");// Cargar el driver
		// Establecemos la conexion con la BD
		Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost/HR","user1", "pass1");

                String sql = " CREATE FUNCTION nombre_dep35(dep INT) RETURNS VARCHAR(15) DETERMINISTIC BEGIN DECLARE nom VARCHAR(15) DEFAULT \"INEXISTENT\"; SELECT dnombre INTO nom FROM departamentos WHERE dept_no = dep; RETURN nom; END";

       
                Statement sentencia = conexion.createStatement();
                int filas = sentencia.executeUpdate(sql);

	        System.out.printf("Resultat de l'execuci�: %d %n", filas);

		sentencia.close(); // Cerrar Statement
		conexion.close(); // Cerrar conexi�n

	}// fin de main
}// fin de la clase
