package classes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BasesDeDatos {
    private final String URL = "jdbc:mysql://localhost:3306/"; // Ubicación de la BD.
    private final String BD = "empresa"; // Nombre de la BD.
    private final String USER = "user1";
    private final String PASSWORD = "pass1";

    public Connection conexion = null;

    @SuppressWarnings("finally")
    public Connection conectar() throws SQLException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection(URL + BD, USER, PASSWORD);
            if (conexion != null) {
                System.out.println("¡Connexió feta!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return conexion;
        }
    }
}
